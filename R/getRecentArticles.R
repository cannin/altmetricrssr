#' Get recent articles from PubMed for a journal
#'
#' @param journal name of journal
#' @param getImages boolean get images
#' @param offsetDays days back
#' @param retmax max articles to grab
#'
#' @return a data.frame with results
#'
#' @importFrom httr GET content
#' @importFrom xml2 xml_find_all xml_find_first read_xml xml_text write_xml xml_find_first
#' @export
getRecentArticles <- function(journal="PLoS biology", getImages=FALSE, offsetDays=180, retmax=10) {
  #journal <- "Proc Natl Acad Sci U S A"
  #getImages <- TRUE

  #Sys.setenv("DEBUG_SIMPLERCACHE"="TRUE")
  #setCacheRootPath("cache")
  #GETCached <- addMemoization(GET)

  #mindate <- as.Date("2017-06-01")
  mindate <-  Sys.Date() - offsetDays
  maxdate <- mindate + 7
  mindate <- url_escape(format(mindate, format="%Y/%m/%d"))
  maxdate <- url_escape(format(maxdate, format="%Y/%m/%d"))

  # Search PubMed
  query <- paste0('("', journal, '"[journal]) AND (journal article [pt]) AND hasabstract')
  q2 <- url_escape(query)
  url <- paste0('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&usehistory=y&datetype=crdt&retmax=', retmax, '&rettype=XML&term=',q2,"&mindate=",mindate,"&maxdate=",maxdate)
  url
  cat("URL: ", url, "\n")

  #tmpEsearchDat <- url %>% GET() %>% content("text")
  tmpEsearchDat <- getURL(url)
  tmpEsearchDat
  
  esearchDat <- read_xml(tmpEsearchDat, verbose=TRUE)

  ## Use PubMed WebEnv to retrieve results
  t1 <- xml_find_all(esearchDat, "//WebEnv")
  webenv <- xml_text(t1)
  webenv

  # Fetch PubMed content
  url <- paste0('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&query_key=1&rettype=xml&WebEnv=',url_escape(webenv))
  url
  cat("URL: ", url, "\n")

  #tmpEfetchDat <- url %>% GET() %>% content("text")
  tmpEfetchDat <- getURL(url)
  tmpEfetchDat
  #efetchDat <- read_xml("efetch.xml")
  efetchDat <- read_xml(tmpEfetchDat, verbose=TRUE)
  write_xml(efetchDat, "efetch.xml")

  articles <- xml_find_all(efetchDat, "//PubmedArticle")

  # Filter number of articles
  if(length(articles) > retmax) {
    articles <- articles[1:retmax]
  }

  cat("Article Count: ", length(articles), "\n")

  tmp <- data.frame(title=character(0),
                    journal=character(0),
                    abstract=character(0),
                    author=character(0),
                    pmid=character(0),
                    doi=character(0),
                    pmc=character(0),
                    altmetric=numeric(0),
                    altmetricSimilarAgePct=numeric(0),
                    altmetricSimilarAgeJournalPct=numeric(0),
                    img=character(0),
                    mesh=character(0),
                    hasMesh=logical(0),
                    articleDate=character(0),
                    visited=character(0),
                    retrievedDate=character(0),
                    stringsAsFactors=FALSE,
                    check.names=FALSE)

  for(i in 1:length(articles)) {
    #i <- 11

    article <- articles[[i]]

    title <- xml_text(xml_find_all(article, ".//ArticleTitle"))
    title

    journal <- xml_text(xml_find_all(article, ".//Journal/Title"))
    journal
    
    abstract <- xml_text(xml_find_all(article, ".//AbstractText"))
    abstract <- ifelse(length(abstract) > 1, paste(abstract, collapse = " "), abstract)
    abstract

    #abstract <- xml_text(xml_find_first(article, ".//AbstractText"))
    #abstract
    
    author <- xml_text(xml_find_all(article, ".//AuthorList/Author/LastName"))
    if(length(author) == 1) {
      author <- author 
    } else if(length(author) > 1) {
      author <- author[c(1,length(author))]
    } else {
      author <- ""
    }
    author <- ifelse(length(author) > 1, paste(author, collapse = " ... "), author)
    author

    pmid <- xml_text(xml_find_first(article, ".//ArticleId[@IdType='pubmed']"))
    pmid

    doi <- xml_text(xml_find_all(article, ".//ArticleId[@IdType='doi']"))
    doi

    pmc <- xml_text(xml_find_all(article, ".//ArticleId[@IdType='pmc']"))
    pmc

    articleYear <- xml_text(xml_find_all(article, ".//ArticleDate/Year"))
    articleMonth <- xml_text(xml_find_all(article, ".//ArticleDate/Month"))
    articleDay <- xml_text(xml_find_all(article, ".//ArticleDate/Day"))
    articleYear
    articleMonth
    articleDay
    
    pubmedpubYear <- xml_text(xml_find_all(article, ".//PubMedPubDate[@PubStatus='accepted']/Year"))
    pubmedpubMonth <- xml_text(xml_find_all(article, ".//PubMedPubDate[@PubStatus='accepted']/Month"))
    pubmedpubDay <- xml_text(xml_find_all(article, ".//PubMedPubDate[@PubStatus='accepted']/Day"))
    pubmedpubYear
    pubmedpubMonth
    pubmedpubDay
    
    year <- ifelse(length(articleYear) == 1, articleYear, pubmedpubYear)
    month <- ifelse(length(articleMonth) == 1, articleMonth, pubmedpubMonth)
    day <- ifelse(length(articleDay) == 1, articleDay, pubmedpubDay)
    year 
    month
    day

    # NOTE: HACK: PMID IS ASSUMED TO ALWAYS BE PRESENT
    if(length(doi) != 1 ||
       length(abstract) != 1 ||
       length(year) != 1 ||
       length(month) != 1 ||
       length(day) != 1 ||
       is.na(abstract) ||
       is.na(year) ||
       is.na(month) ||
       is.na(day)) {
      cat("WARNING: ISSUE SKIP ARTICLE: PMID: ", pmid, "\n")
      next
    }

    if(length(pmc) == 0) {
      pmc <- ""
    }

    articleDate <- as.Date(paste0(year, "-", month, "-", day))
    cat("I: ", i, " Date: ", as.character(articleDate), ", PMID: ", pmid, "\n")

    tmpAltmetric <- getAltmetrics(doi)
    altmetric <- tmpAltmetric$score
    altmetricSimilarAgePct <- tmpAltmetric$similarAgePct
    altmetricSimilarAgeJournalPct <- tmpAltmetric$similarAgeJournalPct

    img <- NULL
    if(getImages) {
      img <- tryCatch({
        getPubmedImage(pmid, returnBase64 = FALSE)
      }, error=function(e) {
        cat("WARNING: IMG NOT FOUND: PMID: ", pmid, "\n")
      })
    }

    if(is.null(img)) {
      img <- ""
    }

    mesh <- xml_text(xml_find_all(article, ".//DescriptorName"))
    mesh <- paste(mesh, collapse = "|")
    mesh

    hasMesh <- FALSE

    visited <- ""
    retrievedDate <- as.character(Sys.Date())

    t2 <- data.frame(title=title,
                     journal=journal,
                     abstract=abstract,
                     author=author,
                     pmid=pmid,
                     doi=doi,
                     pmc=pmc,
                     altmetric=altmetric,
                     altmetricSimilarAgePct=altmetricSimilarAgePct,
                     altmetricSimilarAgeJournalPct=altmetricSimilarAgeJournalPct,
                     img=img,
                     mesh=mesh,
                     hasMesh=hasMesh,
                     articleDate=as.character(articleDate),
                     visited=visited,
                     retrievedDate=retrievedDate,
                     stringsAsFactors=FALSE,
                     check.names=FALSE)

    if(!is.na(abstract)) {
      tmp <- rbind(tmp, t2)
    }

    #cat("CUR JOURNAL ARTICLE COUNT: ", nrow(tmp), "\n")
  }

  return(tmp)
}
