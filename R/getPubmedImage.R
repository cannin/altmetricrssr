#' Get PubMed Image 
#' 
#' @param pmid a PMID 
#' @param returnBase64 whether to return a base64 version of image rather than link
#' @param sleep number of seconds to sleep
#' 
#' @return a link (or base64) to an image 
#' 
#' @examples 
#' getPubmedImage("26685306", returnBase64=TRUE)
#' 
#' @concept altimetricrssR
#' 
#' @importFrom knitr image_uri 
#' @importFrom XML xpathSApply xmlGetAttr
#' @importFrom httr GET write_disk 
#' @export
getPubmedImage <- function(pmid, returnBase64=TRUE, sleep=1) {
  if(pmid == "") {
    return("")
  }
  
  url <- paste0("https://www.ncbi.nlm.nih.gov/pmc/?term=", pmid, "%5BPMID%5D&report=imagesdocsum")
  
  #req <- GET(url)
  #tmpXml <- content(req, as="text")
  tmpXml <- getURL(url)
  
  if(tmpXml == "") {
    return("")
  }
  
  doc <- htmlTreeParse(tmpXml, useInternalNodes=TRUE)
  
  imgs <- xpathSApply(doc, "//a[@class='rprt_img figpopup imagepopup']/img", xmlGetAttr, 'src-large')

  if(length(imgs) > 0) {
    imgs <- paste0("https://www.ncbi.nlm.nih.gov", imgs[1])
    
    # TODO FIX THIS
    # If manual fix: db.getCollection('feedburner').update({"imgSrc": "https://www.ncbi.nlm.nih.gov/pmc/articles/instance/4209186/bin/nihms-610180-f0012.jpg"}, {"$set":{"imgSrc": "", "imgContent": "", "hasImg": false }}, { "multi": true })
    if(imgs == "https://www.ncbi.nlm.nih.gov/pmc/articles/instance/4209186/bin/nihms-610180-f0012.jpg") {
      imgs <- ""
    }
  }
  
  if(is.null(imgs)) {
    imgs <- ""
  } else {
    if(returnBase64) {
      #Sys.setenv("DEBUG_SIMPLERCACHE"="TRUE")
      #setCacheRootPath("cache")
      #GETCached <- addMemoization(GET)
      
      imgPath <- tempfile(fileext=".jpg")
      
      if(!file.exists(imgPath)) {
        #GET(imgs, write_disk(imgPath, overwrite=TRUE))
        #url <- "https://i.stack.imgur.com/WNbN9.png"
        #content <- getBinaryURL(imgs)
        #writeBin(content, con = imgPath)
        download.file(imgs, imgPath)
      }
      
      imgs <- image_uri(imgPath)
      
      #Sys.sleep(sleep)
    }
  }

  return(imgs)
}


  