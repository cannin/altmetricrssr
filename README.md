# Run
## Run HTTP Server
```
docker rm -f ar; docker run --restart=always --name ar --expose 8080 -e VIRTUAL_HOST=ar2.lunean.com -e PAGESPEED=0 -v /home/ubuntu/efs/workspace/rss:/data -w /data -t cannin/nodejs-http-server:0.10.25
```

## Access site
http://ar2.lunean.com/rss.xml

# Install package starting from container with it (cannin/webscrape is without)
```
docker rm -f ar2; docker run --rm -i --name ar2 -v /home/ubuntu/efs:/data -w /data -t cannin/webscrape:ar bash
Rscript -e 'install.packages("/data/altmetricrssr_0.0.60.tar.gz", repos = NULL, type="source")'
docker commit 5a94320390d4 cannin/webscrape:ar
```

## Without pre-installation
```
Rscript -e 'install.packages(c("R.utils", "xml2"))'
docker commit 7f1706fb0823 cannin/webscrape:ar
```

# Re-run crawl
```
docker rm -f ar3; docker run -i --name ar3 -v /home/ubuntu/efs:/data -w /data -t cannin/webscrape:ar bash
R -e "source(system.file('scripts', 'RunGetRssPubmed.R', package='altmetricrssr'))"
## Ignore (INITIALIZATION MESSAGE): In xmlRoot.XMLInternalDocument(currentNodes[[1]]) : empty XML document
R -e "source(system.file('scripts', 'RunGetXml.R', package='altmetricrssr'))"
```

# Interactive session
```
docker rm -f ar4; docker run -i --name ar4 -v /home/ubuntu/efs:/data -w /data -t cannin/webscrape:ar bash
```

# Package Path
/usr/local/lib/R/site-library

# Get MESH
```
docker rm -f ar2; docker run --name ar2 -v /home/ubuntu/efs/workspace/rss:/data -w /data -t cannin/webscrape:ar sh -c "R -e \"source(system.file('scripts', 'RunGetMeshMti.R', package='altmetricrssr'))\""
```
