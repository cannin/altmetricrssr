library(XML, quietly=TRUE)
library(altmetricrssr, quietly=TRUE)
session <- capture.output(sessionInfo())
cat("SESSION: ", session, "\n")

efsDir <- "/data"

if(file.exists(efsDir)) { 
  cat("D/data: ", dir(efsDir), "\n")
  setwd(efsDir) 
}
cat("D: ", dir(), "\n")
cat("WD: ", getwd(), "\n")

df <- readRDS("rss.rds")
tmpDfVisited <- df[(df$visited != ""),]
tmpDf <- df[(df$visited == ""),]
keepCols <- colnames(tmpDf)

# PARAMETERS ----
maxMeshValue <- 5
maxImgValue <- 1

#selectedMeshFile <- "selectedMesh.txt"
#ignoredMeshFile <- "ignoredMesh.txt"
selectedMeshFile <- "selectedMesh_expanded.txt"
ignoredMeshFile <- "ignoredMesh_expanded.txt"
neutralMeshFile <- "neutralMesh.txt"

# GET MESH PATH ----

if(file.exists(selectedMeshFile)) { 
  meshDir <- getwd() 
} else if(file.exists("inst/extdata")) { 
  meshDir <- "inst/extdata" 
} else {
  meshDir <- system.file("extdata", package="altmetricrssr")
}

cat("MESHDIR: ", meshDir, "\n")

selectedMeshPath <- file.path(meshDir, selectedMeshFile)
ignoredMeshPath <- file.path(meshDir, ignoredMeshFile)
neutralMeshPath <- file.path(meshDir, neutralMeshFile)

## RSS parameters
maxXmlEntries <- 2

title <- "Altmetric Ranked Biomedical Research Articles"
description <- "Delivering biomedical research articles based on Altmetric scores that measure the quality and quantity of attention that articles receive."
link <- "http://lunean.com"
# Wed, 02 Oct 2002 08:00:00 EST
date <- format(Sys.time(), "%a, %d %b %Y %H:%M:%S %Z")

# GET READABLE MESH ----
tmpDf$meshScore <- 0

selectedMesh <- readLines(selectedMeshPath)
ignoredMesh <- readLines(ignoredMeshPath)
neutralMesh <- readLines(neutralMeshPath)

selectedMesh <- setdiff(selectedMesh, neutralMesh)
ignoredMesh <- setdiff(ignoredMesh, neutralMesh)
selectedMesh <- setdiff(selectedMesh, ignoredMesh)

# JOURNAL STATS ----
journalStats <- aggregate(df$altmetric, list(df$journal), FUN='quantile', probs=0.8)
t1 <- table(df$journal)
t2 <- data.frame(journal=names(t1), freq=as.numeric(t1))
colnames(journalStats) <- c("journal", "score")
journalStats <- merge(journalStats, t2)

## Initialize MESH columns
tmpDf$dispMesh <- ""
tmpDf$notDispMesh <- ""
tmpDf$notDispIgnoreMesh <- ""
tmpDf$topPrcntScore <- 0

for(i in 1:nrow(tmpDf)) {
  # i <- 153
  if(!file.exists(efsDir)) { 
    cat("I: ", i, "\n")
  }
  
  mesh <- tmpDf$mesh[i]
  tmpMesh <- strsplit(mesh, "\\|")
  tmpMesh <- tmpMesh[[1]]
  
  if(length(tmpMesh) == 0 || is.na(tmpMesh)) {
    next
  }
  
  a <- tolower(tmpMesh)
  b <- tolower(selectedMesh)
  c <- tolower(ignoredMesh)

  I <- length(intersect(a, b))
  #S <- I/(length(a)+length(b)-I)
  #S
  # Maximum 
  # From: https://www.mathsisfun.com/algebra/infinite-series.html
  if(I >= 1) {
    S <- maxMeshValue * sum(sapply(1:I, function(k) { 1/2^k }))
  } else {
    S <- 0
  }
  
  J <- length(intersect(a, c))
  #NS <- I/(length(a)+length(c)-I)
  #NS
  if(J >= 1) {
    NS <- maxMeshValue * sum(sapply(1:J, function(k) { 1/2^k }))
  } else {
    NS <- 0
  }
  
  tmpDf$meshScore[i] <- S - NS
  
  t1 <- intersect(a, b)
  t2 <- setdiff(setdiff(a,b), c)
  t3 <- intersect(a,c)
  
  tmpDf$dispMesh[i] <- paste(t1, collapse="|")
  tmpDf$notDispMesh[i] <- paste(t2, collapse="|")
  tmpDf$notDispIgnoreMesh[i] <- paste(t3, collapse="|")
}

# SORT DATA.FRAME ----
tmpDf$altCat <- 1
journals <- unique(tmpDf$journal)

for(journal in journals) {
  #journal <- "Nature methods"
  idx <- which(tmpDf$journal == journal)
  t1 <- range01(tmpDf[idx, "altmetric"])
  
  if(length(idx) == 1) {
    tmpDf[idx, "altCat"] <- 1
  } else {
    tmpDf[idx, "altCat"] <- as.numeric(cut(t1, breaks=c(-0.1, 0.01, 0.1, 0.25, 0.5, 1), labels=1:5))  
  }
  
  percentile <- ecdf(tmpDf[idx, "altmetric"])
  
  for(i in idx) {
    tmpDf[i, "topPrcntScore"] <- round(percentile(tmpDf[i, "altmetric"]), 2)
  }
}

tmpDf$hasImg <- 0
tmpDf[nchar(tmpDf$img) > 0, "hasImg"] <- maxImgValue

tmpDf$altTmp <- (tmpDf$altmetricSimilarAgePct + tmpDf$altmetricSimilarAgeJournalPct)/100

#tmpDf$score <- rowSums(tmpDf[, c("altCat", "hasImg", "meshScore", "topPrcntScore")])
tmpDf$score <- rowSums(tmpDf[, c("hasImg", "meshScore", "altTmp")])

tmpDf <- tmpDf[with(tmpDf, order(-score)), ]

# BUILD XML TREE ----
xml <- xmlTree()
# Ignore In xmlRoot.XMLInternalDocument(currentNodes[[1]]) : empty XML document here
xml$addTag("rss", close=FALSE, attrs=c(version="2.0"))

xml$addTag("channel", close=FALSE)
xml$addTag("title", title)
xml$addTag("description", description)
xml$addTag("link", link)
xml$addTag("lastBuildDate", date)
xml$addTag("pubDate", date)
xml$addTag("generator", paste0("altmetricrssr: ", packageVersion("altmetricrssr")))
xml$addTag("ttl", 1)

curCount <- 0

for (i in 1:nrow(tmpDf)) {
  abstract <- tmpDf$abstract[i]
  author <- tmpDf$author[i]
  title <- tmpDf$title[i]
  doi <- tmpDf$doi[i]
  altmetric <- tmpDf$altmetric[i]
  journal <- tmpDf$journal[i]
  visited <- tmpDf$visited[i]
  img <- tmpDf$img[i]
  pmc <- tmpDf$pmc[i]
  pmid <- tmpDf$pmid[i]
  dispMesh <- tmpDf$dispMesh[i]
  notDispMesh <- tmpDf$notDispMesh[i]
  articleDate <- tmpDf$articleDate[i]
  topPrcntScore <- tmpDf$topPrcntScore[i]
  altmetricSimilarAgeJournalPct <- tmpDf$altmetricSimilarAgeJournalPct[i]
  
  if(is.na(abstract) || nchar(abstract) < 10) { next }
  
  if(curCount == maxXmlEntries) { break }
  
  #title <- ifelse(topPrcntScore >= 0.9, paste0("*", title), title)
  title <- ifelse(altmetricSimilarAgeJournalPct >= 90, paste0("*", title), title)
  
  abstract <- paste0("Altmetric: ", altmetric, 
                     " :: Journal: ", journal, 
                     " :: Date: ", articleDate, 
                     '<br/>PMID: <a href="https://www.ncbi.nlm.nih.gov/pubmed/', pmid, '">Link</a>', 
                     ' :: PMC: <a href="https://www.ncbi.nlm.nih.gov/pmc/articles/', pmc, '">Link</a>',
                     ' :: Sci-Hub: <a href="http://sci-hub.tw/', doi, '">Link</a>',
                     "<br/>Selected MeSH: ", dispMesh,
                     "<br/>Other MeSH: ", notDispMesh,
                     "<br/><img src='", img, "'/>",
                     "<br/>Abstract: ", abstract)
  
  cat("ABSTRACT: ", abstract, "\n")

  # Create item ----
  xml$addTag("item", close=FALSE)
  xml$addTag("title", title)  
  
  if(!is.na(author)) {
    xml$addTag("author", author)  
  }
  
  xml$addTag("description", close=FALSE)
  xml$addCData(abstract)
  xml$closeTag()
  xml$addTag("link", paste0("http://dx.doi.org/", doi))  
  xml$addTag("pubDate", date)  
  xml$addTag("guid", paste0("http://dx.doi.org/", doi))  
  xml$closeTag()
  
  curCount <- curCount + 1
  tmpDf[tmpDf$doi == doi, "visited"] <- as.character(Sys.time())
}

xml$closeTag()
xml$closeTag()

# SAVE OUTPUT ----

## View XML generated
#cat(saveXML(xml))

saveXML(xml, file="rss.xml", indent=TRUE)

saveRDS(tmpDf, "rss_all.rds")
tmpDfSm <- tmpDf[ , (names(tmpDf) %in% keepCols)]
allDf <- rbind(tmpDfVisited, tmpDfSm) 
saveRDS(allDf, "rss.rds")

# Read date
#y <- as.POSIXct("2017-04-30 13:39:48")

# EXAMPLE RSS ----
# <?xml version="1.0" encoding="UTF-8"?>
# <rss version="2.0">
#   <channel>
#     <title>Altmetric Ranked Biomedical Research Articles</title>
#     <description>Delivering biomedical research articles based on Altmetric scores that measure the quality and quantity of attention that articles receive.</description>
#     <link>http://lunean.com</link>
#     <lastBuildDate>Fri, 26 Feb 2016 05:09:25 GMT</lastBuildDate>
#     <pubDate>Fri, 26 Feb 2016 05:09:25 GMT</pubDate>
#     <ttl>1</ttl>
#     <generator>Meteor RSS Feed</generator>
#     <item>
#       <title><![CDATA[Cockroaches in crevices and confined spaces [Biophysics and Computational Biology]]]></title>
#       <description><![CDATA[Jointed exoskeletons permit rapid appendage-driven locomotion but retain the soft-bodied, shape-changing ability to explore confined environments. We challenged cockroaches with horizontal crevices smaller than a quarter of their standing body height. Cockroaches rapidly traversed crevices in 300-800 ms by compressing their body 40-60%. High-speed videography revealed crevice negotiation to be a complex, discontinuous maneuver. After traversing horizontal crevices to enter a vertically confined space, cockroaches crawled at velocities approaching 60 cm⋅s(-1), despite body compression and postural changes. Running velocity, stride length, and stride period only decreased at the smallest crevice height (4 mm), whereas slipping and the probability of zigzag paths increased. To explain confined-space running performance limits, we altered ceiling and ground friction. Increased ceiling friction decreased velocity by decreasing stride length and increasing slipping. Increased ground friction resulted in velocity and stride length attaining a maximum at intermediate friction levels. These data support a model of an unexplored mode of locomotion-&quot;body-friction legged crawling&quot; with body drag, friction-dominated leg thrust, but no media flow as in air, water, or sand. To define the limits of body compression in confined spaces, we conducted dynamic compressive cycle tests on living animals. Exoskeletal strength allowed cockroaches to withstand forces 300 times body weight when traversing the smallest crevices and up to nearly 900 times body weight without injury. Cockroach exoskeletons provided biological inspiration for the manufacture of an origami-style, soft, legged robot that can locomote rapidly in both open and confined spaces.<br/><br/>Journal: Proceedings of the National Academy of Sciences current issue<br/>Altmetric: 1068.12<br/>PubMed: <a href='http://www.ncbi.nlm.nih.gov/pubmed/26858443'>26858443</a><br/>DOI: 10.1073/pnas.1514591113]]></description>
#       <link>http://dx.doi.org/10.1073/pnas.1514591113</link>
#       <pubDate>Fri, 26 Feb 2016 04:37:59 GMT</pubDate>
#       <guid>http://dx.doi.org/10.1073/pnas.1514591113</guid>
#     </item>
#   </channel>
# </rss>
