library(altmetricrssr)
library(plyr)
session <- capture.output(sessionInfo())
cat("SESSION: ", session, "\n")

efsDir <- "/data"

if(file.exists(efsDir)) { 
  cat("D/data: ", dir(efsDir), "\n")
  setwd(efsDir) 
}

# nrm; https://www.nature.com/articles/palcomms201516/figures/2
journals <- read.table(system.file("extdata/journals.txt", package = "altmetricrssr"),
                       sep="\t", header=TRUE, stringsAsFactors = FALSE)

#journals <- journals[1:5,]

# Parameters 
getImages <- TRUE

tmp <- data.frame(title=character(0),
                  journal=character(0),
                  abstract=character(0),
                  author=character(0),
                  pmid=character(0),
                  doi=character(0),
                  pmc=character(0),
                  altmetric=numeric(0),
                  altmetricSimilarAgePct=numeric(0),
                  altmetricSimilarAgeJournalPct=numeric(0),
                  img=character(0),
                  mesh=character(0),
                  hasMesh=logical(0),
                  articleDate=character(0),
                  visited=character(0),
                  retrievedDate=character(0),
                  stringsAsFactors=FALSE,
                  check.names=FALSE)

# DEBUG
#journals <- journals[(length(journals)-1):length(journals)]
#journals <- journals[journals$journal %in% c("Science"), ]

for(i in 1:nrow(journals)) {
  #i <- 1
  journal <- journals[i, "journal"]
  offsetMonths <- journals[i, "free_access_months"]

  if(!is.na(offsetMonths) && offsetMonths > 0) {
    offsetDays <- offsetMonths * 30
  } else {
    offsetDays <- 7
  }

  cat("JOURNAL: ", journal, " OFFSETDAYS: ", offsetDays, "\n")

  results <- NULL
  
  # Sleep time for API 
  tic <- Sys.time()
  
  tryCatch({
    results <- getRecentArticles(journal=journal, getImages=getImages, offsetDays=offsetDays, retmax=30)

    if (!is.null(results)) {
      cat("CurData1: Rows: ", nrow(tmp), " Cols: ", ncol(tmp), "\n")
      #insertRss(results)
      tmp <- rbind(tmp, results)
      cat("NewData2: Rows: ", nrow(tmp), " Cols: ", ncol(tmp), "\n")
    }
  }, error = function(e) {
    cat("WARNING: getRecentArticles() with journal: ", journal, "\n")
    message(e)
  })
  
  # Sleep time for API
  sleepTime <- 30
  toc <- Sys.time()
  sleepTime <- sleepTime - difftime(toc, tic, units = "secs")
  sleepTime <- ifelse(sleepTime > 0, sleepTime, 0)
  Sys.sleep(sleepTime)
}

allResults <- unique(tmp)

cleanResults <- function(path, allResults) {
  tmp <- readRDS(path)
  allResults <- rbind.fill(allResults, tmp)

  # Replace retrievedDate to today's date
  curDois <- allResults[allResults$retrievedDate == as.character(Sys.Date()), "doi"]
  allResults[allResults$doi %in% curDois, "retrievedDate"] <- as.character(Sys.Date())
  allResults[is.na(allResults$retrievedDate), "retrievedDate"] <- as.character(Sys.Date())

  # Remove duplicates
  uniqDoi <- unique(allResults$doi)

  for(i in 1:length(uniqDoi)) {
    #i <- 1
    idx <- which(allResults$doi == uniqDoi[i])

    if(length(idx) > 1) {
      allResults$altmetric[idx] <- max(allResults$altmetric[idx])
      allResults$altmetricSimilarAgePct[idx] <- max(allResults$altmetricSimilarAgePct[idx])
      allResults$altmetricSimilarAgeJournalPct[idx] <- max(allResults$altmetricSimilarAgeJournalPct[idx])
      
      allResults <- allResults[-idx[2:length(idx)], ]
    }
  }

  nrow(allResults)
  allResults <- unique(allResults)
  nrow(allResults)

  saveRDS(allResults, path)
  
  return(allResults)
}

if(file.exists("rss.rds")) {
  allResults <- cleanResults("rss.rds", allResults)
} else {
  saveRDS(allResults, "rss.rds")
}

# x <- readRDS("rss.rds.bak")
#
# allResults[, "visited"] <- ""
# allResults[allResults$retrievedDate != Sys.Date(), "visited"] <- Sys.Date()
