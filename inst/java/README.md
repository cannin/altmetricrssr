../compile.sh GenericBatchUser2.java

java -cp ../classes:../lib/skrAPI.jar:../lib/commons-logging-1.1.1.jar:../lib/httpclient-cache-4.1.1.jar:../lib/httpcore-nio-4.1.jar:../lib/httpclient-4.1.1.jar:../lib/httpcore-4.1.jar:../lib/httpmime-4.1.1.jar GenericBatchUser

# Build Single JAR
ant compile
ant onejar

# Run JAR
java -cp skrapi.jar org.lunean.GenericBatchUser
javap -cp skrapi.jar org.lunean.GenericBatchUser

80N9EqJYUD
