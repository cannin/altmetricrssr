#' Get Cell RSS feed 
#' 
#' @param url a URL 
#' @param getImages get image from PubMed 
#' 
#' @return a data.frame with results 
#' 
#' @concept altimetricrssR
#' @importFrom XML xmlTreeParse xpathApply xmlValue
#' @export
getCell <- function(url, getImages=FALSE) {
  namespaces <- c(ns="http://purl.org/rss/1.0/",
                  dc="http://purl.org/dc/elements/1.1/",
                  content="http://purl.org/rss/1.0/modules/content/")
  
  req <- curl_fetch_memory(url)
  tmpXml <- rawToChar(req$content)
  
  doc <- xmlTreeParse(tmpXml, useInternalNodes=TRUE)
  
  journal <- xpathApply(doc, "//ns:channel/ns:title", xmlValue, namespaces=namespaces)[[1]]
  
  title <- xpathApply(doc, "//ns:item/ns:title", xmlValue, namespaces=namespaces)
  
  doi <- xpathApply(doc, "//ns:item/dc:identifier", xmlValue, namespaces=namespaces)
  #doi <- gsub("^doi:", "", doi)
  
  content <- xpathApply(doc, "//ns:item/ns:description", xmlValue, namespaces=namespaces)
  
  #author <- xpathApply(doc, "//ns:entry/ns:author/ns:name", xmlValue, namespaces=namespaces)
  
  altmetric <- getAltmetrics(doi)
  
  pm <- getAbstract(doi)
  
  if(getImages) {
    img <- tryCatch({
      img <- sapply(pm$pmid, function(x) {
        tmp <- getPubmedImage(x, returnBase64 = TRUE)    
      }, simplify = TRUE)
      
      unname(img)
    }, error=function(e) {
      cat("WARNING: IMG\n")
    })
  } else {
    img <- rep("", length(doi))
  }
  
  mesh <- rep("", length(doi))
  allMesh <- rep("", length(doi))
  visited <- rep("", length(doi))
  retrievedDate <- rep(as.character(Sys.Date()), length(doi))
  
  results <- data.frame("_id"=unlist(doi),
                        title=unlist(title),
                        doi=unlist(doi),
                        journal=rep(journal, length(doi)),
                        altmetric=unlist(altmetric),
                        content=unlist(content),
                        abstract=pm$abstract,
                        pmid=pm$pmid, 
                        img=img,
                        mesh=mesh,
                        allMesh=allMesh,
                        visited=visited,
                        retrievedDate=retrievedDate, 
                        stringsAsFactors=FALSE,
                        check.names = FALSE)
  
  return(results)
}