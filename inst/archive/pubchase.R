library(rmongodb) 
library(rvest)
library(jsonlite)
library(curl)

# Example
#json <- '{"a":1, "b":2, "c": {"d":3, "e":4}}'

# CRON 
#0 4 * * * Rscript /Users/lunaa/Dropbox/drug_target_tmp/mongodbExample/pubchase.R

#m <- mongo.create(host="localhost:27017")
coll <- "admin.pubchase"

m <- mongo.create(host="ixl.ddns.net:27017")
#mongo.drop(m, coll)

stopifnot(mongo.is.connected(m))

getPubchase <- function(researchArea, sleepTime=3) {
    cat("RESEARCH AREA: ", researchArea, "\n")
    
    tmp <- html(paste0("https://www.pubchase.com/explore/", researchArea))
    tmp1 <- html_nodes(tmp, xpath='//*[@class="rs_go_to_article"]')
    titles <- html_text(tmp1)
    
    for(title in titles) {
        title <- gsub('"', "&quot;", title)
        title <- gsub("'", "&apos;", title)
        title <- gsub("<", "&lt;", title)
        title <- gsub(">", "&gt;", title)
        title <- gsub("&", "&amp;", title)
        
        isNewTitle <- is.null(mongo.find.one(m, coll, paste0('{"title":"', title, '"}')))
        
        if(TRUE) {
        #if(isNewTitle) {
            Sys.sleep(sleepTime)
            
            term <- URLencode(title)
            baseUrl <- "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&retmode=json&retmax=1&term="
            url <- paste0(baseUrl, term)
            
            cat("URL: ", url, "\n")
            
            result <- fromJSON(url)
            pubmedId <- result$esearchresult$idlist
            
            if(length(pubmedId) == 1) {
                #         fetchBaseUrl <- "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&retmode=text&rettype=abstract&id="
                #         fetchUrl <- paste0(fetchBaseUrl, pubmedId)
                #         req <- curl_fetch_memory(fetchUrl)
                #         abstract <- rawToChar(req$content)
                #         
                #         cat("ABSTRACT: ", abstract, "\n")
                #         formattedAbstract <- gsub("\\n", "<br/>", abstract)
                
                # http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&rettype=abstract&id=213213
                fetchBaseUrlXml <- "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&rettype=abstract&id="
                fetchUrlXml <- paste0(fetchBaseUrlXml, pubmedId)
                
                req <- curl_fetch_memory(fetchUrlXml)
                tmpXml <- rawToChar(req$content)
                
                doc <- xmlTreeParse(tmpXml, useInternalNodes=TRUE)
                
                journal <- xpathApply(doc, "//Title", xmlValue)[[1]]
                tmpResult <- xpathApply(doc, "//AbstractText", xmlValue)
                
                if(length(tmpResult) == 1) {
                    abstract <- tmpResult[[1]]
                    
                    abstract <- gsub('"', "", abstract)
                    abstract <- gsub('"', "&quot;", abstract)
                    abstract <- gsub("'", "&apos;", abstract)
                    abstract <- gsub("<", "&lt;", abstract)
                    abstract <- gsub(">", "&gt;", abstract)
                    abstract <- gsub("&", "&amp;", abstract)
                    
                    formattedAbstract <- paste0(abstract, "<br/><br/>Journal: ", journal)
                    
                    
                    listTmp <- list(title=title, pmid=pubmedId, timestamp=Sys.time(), 
                                    researchArea=researchArea, abstract=formattedAbstract,
                                    rnd=runif(1), shown=FALSE)
                    #json <- toJSON(listTmp)
                    #bson <- mongo.bson.from.JSON(json)
                    
                    bson <- mongo.bson.from.list(listTmp)
                    mongo.insert(m, coll, bson)                     
                }
            }
        } else {
            cat("NOT NEW\n")
        }
    }
}

researchAreas <- c("biochemistry", "biophysics", "cancer-biology", "cell-biology", "chemistry", 
                   "clinical-research", "computational-biology", "developmental-biology", "evolutionary-biology", "genetics", 
                   "genomics", "immunology", "microbiology", "neurobiology", "neuroscience", 
                   "population-genetics", "protein-folding", "psychology", "single-molecule", "stem-cells", 
                   "structural-biology", "synthetic-biology", "systems-biology", "virology")

researchAreas <- c("biochemistry", "cancer-biology", "cell-biology", 
                   "clinical-research", "computational-biology", 
                   "genomics", 
                   "systems-biology")

#sapply(researchAreas[1:length(researchAreas)], getPubchase)
sapply(sample(researchAreas), getPubchase)


