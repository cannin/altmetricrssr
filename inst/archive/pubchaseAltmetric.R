#UPDATE: db.getCollection('pubchase').update({"shown":true}, {$set: {"shown": false}}, {multi: true, upsert: false})
#SEARCH: db.getCollection('pubchase').find({"shown":true})
#SEARCH/COUNT: db.getCollection('pubchase').find({"shown":true}).count()
#SEARCH/SORT/LIMIT: db.getCollection('pubchase').find({ shown: false }).sort({"rnd": -1}).limit(2)

library(rmongodb) 
library(httr)
library(plyr)

m <- mongo.create(host="ixl.ddns.net:27017")
mongo.is.connected(m)

# NOTE: Does nothing ??? 
mongo.get.databases(m) 

mongo.get.database.collections(m, "admin")

coll <- "admin.pubchase"
mongo.count(m, coll)
mongo.find.one(m, coll)

# NOTE: Numbers are not strings
tmp <- mongo.find.all(m, coll)
#print(tmp)

# Record
mongo.bson.to.list(tmp)
# Value
pmids <- lapply(tmp, function(x) { mongo.bson.value(x, "pmid") })

cursor <- mongo.find(m, coll)
pmids <- NULL
scores <- NULL
titles <- NULL

results <- NULL

## iterate over the cursor
while (mongo.cursor.next(cursor)) {
  # iterate and grab the next record
  tmp <- mongo.bson.to.list(mongo.cursor.value(cursor))
  cat("PMID: ", tmp$pmid, "\n")
  
  pmids <- c(pmids, tmp$pmid)
  titles <- c(titles, tmp$title)
  
  r <- GET(paste0("http://dev.lunean.com/raltmetric/", tmp$pmid))
  score <- content(r)
  scores <- c(scores, score)
  Sys.sleep(1)
  
  if(!is.null(score)) {
    results <- rbind(results, c(tmp$pmid, score, tmp$title))
  }
}

t1 <- data.frame(pmids=pmids, scores=scores, titles=titles)
