% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/range01.R
\name{range01}
\alias{range01}
\title{Re-scale between 0 and 1}
\usage{
range01(x)
}
\arguments{
\item{x}{a numeric vector}
}
\value{
a scaled vector
}
\description{
Re-scale between 0 and 1
}
\concept{altmetricrssr}
